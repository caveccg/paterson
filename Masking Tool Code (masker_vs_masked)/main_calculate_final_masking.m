
clc
clear all
close all

%Find masker

%fuction to call a file wave for masker
[file] = readFile();

[y, fs]=audioread(file);

info = audioinfo(file);
nbits = info.BitsPerSample;
Nchanels = info.NumChannels;
bitrate = fs*nbits/Nchanels;
y =  y(:,1);


calibrator = 80; % The SPL we want our file reproduced

% function to find Leq original
[T1, audiogain_dB, audio_proc_L_dB, maxdbL, meandbL] = find_leq (y, 'z',0, fs);

Gain = calibrator - meandbL; %80 db is the SPL we want the audio to be analyzed for,
                             % our calibration value we subtract between the 
                             %leq (z) and find our calibration value
                             
% function to gain a file
[audiogain] = audiogain (y, Gain);

% function weighting 'a' or 'z'
weight = 'z';
x = weighting(audiogain,fs,weight);

Layer = 1;
INDEX = 1;

% 3 - Determination of the absolute threshold.
 
 if (fs == 44100)
   [TH, Map, LTq] = Table_absolute_threshold1(Layer, fs, bitrate); % fs = 44100 hz
    else
   [TH, Map, LTq] = Table_absolute_threshold3(Layer, fs, bitrate); % fs = 48000 hz
    end

   if (fs == 44100)
   CB = Table_critical_band_boundaries2(Layer, fs);% fs = 44100 hz
   else
   CB = Table_critical_band_boundaries1(Layer, fs);% fs = 48000 hz
   end
  

  
% Convert Indice to frenquency  
if (fs == 44100)
     [THF]  = Abs_treshold(fs);% fs = 44100 hz
   else
     [THF]  = Abs_treshold48(fs);% fs = 48000 hz
   end
   
  % Scale Barks 
     if (fs == 44100)
   [SBark] = Table_absolute_threshold_bark_44(Layer, fs);% fs = 44100 hz
   else
   [SBark] = Table_absolute_threshold_bark_48(Layer, fs);% fs = 48000 hz
 end
 
SBark = SBark (:,2);

C = Table_analysis_window_new;


OFFSET = 1:384:length(audiogain) -384;

 
for k =1:length(OFFSET)

 % Step-1 : Spectral analyses (PSD)
 [X(k,:)]  = FFT_Analysis_new(x, OFFSET(k), fs);

 % 2 - Finding of the tonal and non-tonal components of the audio signal.
 [Flags, Tonal_list, Non_tonal_list] = Find_tonal_components_new(X(k,:), TH, Map, CB,fs);
  
 % 3 - Decimation of the maskers to obtain only the relevant maskers.
 [DFlags, DTonal_list, DNon_tonal_list] = Decimation_new(X(k,:), Tonal_list, Non_tonal_list, Flags, TH, Map);

 % 4 Find the individual maskers tonal and non tonal
 [LTt, LTn] = Individual_masking_thresholds_new(X(k,:), Tonal_list, Non_tonal_list, TH, Map);

 % 5 Compute the global masking threshold [1, pp. 114]
 LTgX(k,:) = Global_masking_threshold_new(LTq, LTt, LTn);

end

% 5 find all maximus of all block (OFFSET(k))
LTg = max(LTgX);


%Find masked

%fuction to call a file wave masked
[file1] = readFile();

[y1, fs1]=audioread(file1);

y1 =  y1(:,1);

calibrator = 80; % The SPL we want our file reproduced

% function to find Leq original
[T1, audiogain_dB, audio_proc_L_dB, maxdbL, meandbL] = find_leq (y1, 'z',0, fs1);

Gain1 = calibrator - meandbL; %80 db is the SPL we want the audio to be analyzed for,
                             % our calibration value we subtract between the 
                             %leq (z) and find our calibration value
                             
% function to gain a file
[audiogain1] = audiogain1 (y1, Gain1);

% function weighting 'a' or 'z'
weight1 = 'z';
x1 = weighting(audiogain1,fs1,weight1);

OFFSET1 = 1:384:length(audiogain1) -384;

 
for k =1:length(OFFSET1)

  % Step-1 : Spectral analyses and SPL Normalization
 
[X1(k,:)]  = FFT_Analysis_new(x1, OFFSET1(k), fs);

end


masked = max(X1);
f1 =(fs1/2*linspace(0,1,512/2+1)); % freq vector
ydft1 =(masked(1:length(f1))); % mag vector

THF1 = THF;
THF1(1,:) = [];
LTg1 = LTg(2:end);
SBark1 = SBark(2:end);

% plot figure in logaritmic scale
clf
figure(1);
semilogx(f1, ydft1,'Color',[0. 0.3 1.]);
hold on;
semilogx( THF1(:,1), THF1(:,3), 'Color',[0. 0.7 0.], ':',"linewidth",1);
hold on;
semilogx(THF1(:, INDEX), LTg1,'Color',[1 0. 0.2],"linewidth",1);
line ([SBark1, SBark1], [-40. 120.] ,'linestyle', '--', 'color',[0.5 0.5 0.1],"linewidth",1);
title('LTG masker Vs PSD masked');

h = legend('PSD masked','LTq','LTG masker','Bark Scale','location', 'southeastoutside','orientation', 'vertical'); %// example legend
ch = findobj(get(h,'children'), 'type', 'text'); %// children of legend of type text
set(ch, 'Fontsize', 11); %// set value as desired
ch = findobj(get(h,'children'), 'type', 'line'); %// children of legend of type line
set(ch, 'Markersize', 12); %// set value as desired

legend boxoff ;

h4 = get (gcf, "currentaxes");
set(h4,"fontweight","bold","linewidth",1)
xlabel('Frequency',"fontsize",10); 
ylabel('dB',"fontsize",10); 

xlim([0 20000]);
ylim([-20 100]);

f = get(gca,'XTickLabel');
set(gca,'XTickLabel',f,'FontName','Times','fontsize',10)

hold off;


% plot figure in linear scale
figure(2);
plot(f1, ydft1,'Color',[0. 0.3 1.]);
hold on;
plot( THF(:,1), THF(:,3), 'Color',[0. 0.7 0.], ':',"linewidth",1);
hold on;
plot(THF(:, INDEX), LTg,'Color',[1 0. 0.2],"linewidth",1);
line ([SBark, SBark], [-40. 120.] ,'linestyle', '--', 'color',[0.5 0.5 0.1],"linewidth",1);
title('LTG masker Vs PSD masked');

h = legend('PSD masked','LTq','LTG masker','Bark Scale','location', 'southeastoutside','orientation', 'vertical'); %// example legend
ch = findobj(get(h,'children'), 'type', 'text'); %// children of legend of type text
set(ch, 'Fontsize', 11); %// set value as desired
ch = findobj(get(h,'children'), 'type', 'line'); %// children of legend of type line
set(ch, 'Markersize', 12); %// set value as desired

legend boxoff ;

h4 = get (gcf, "currentaxes");
set(h4,"fontweight","bold","linewidth",1)
xlabel('Frequency',"fontsize",10); 
ylabel('dB',"fontsize",10); 

xlim([0 20000]);
ylim([-20 100]);

f = get(gca,'XTickLabel');
set(gca,'XTickLabel',f,'FontName','Times','fontsize',10)

hold off;



	

  
  
  