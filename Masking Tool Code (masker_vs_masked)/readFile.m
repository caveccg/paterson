function [file] = readFile()
[file,path] = uigetfile('*.wav');
if isequal(file,0)
   disp('User selected Cancel');
else
   disp(['User selected ', fullfile(path,file)]);
end
endfunction