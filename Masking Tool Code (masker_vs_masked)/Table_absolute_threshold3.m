function [TH, Map, LTq] = Table_absolute_threshold3(Layer, fs, bitrate)
%[TH, Map,  LTq] = Table_absolute_threshold(Layer, fs, bitrate)
%
%   Returns the frequencies, critical band rates and absolute threshold
%   in TH. Map contais a mapping beween the frequency line k and an index
%   number for the TH or LTq tables. LTq contains only the threshold in quiet
%   LT_q(k) defined in tables D.1x of the standard [1, pp. 117].
%   
%   These values depends on the Layer, the frequency rate fs (H)z and the
%   bit rate `bitrate' kbits/s.
%
%   Conversion of the frequency f into barks is done using (f * bitrate / fs)
   
%  
%   References:
%    [1] Information technology -- Coding of moving pictures and associated
%        audio for digital storage media at up to 1,5 Mbits/s -- Part3: audio.
%        British standard. BSI, London. October 1993. Implementation of ISO/IEC
%        11172-3:1993. BSI, London. First edition 1993-08-01.
%
%   Legal notice:
%    This computer program is based on ISO/IEC 11172-3:1993, Information
%    technology -- Coding of moving pictures and associated audio for digital
%    storage media at up to about 1,5 Mbit/s -- Part 3: Audio, with the
%    permission of ISO. Copies of this standards can be purchased from the
%    British Standards Institution, 389 Chiswick High Road, GB-London W4 4AL, 
%    Telephone:+ 44 181 996 90 00, Telefax:+ 44 181 996 74 00 or from ISO,
%    postal box 56, CH-1211 Geneva 20, Telephone +41 22 749 0111, Telefax
%    +4122 734 1079. Copyright remains with ISO.
%-------------------------------------------------------------------------------
Common_new;


   
   if (Layer == 1)
   if (fs == 48000)
      % Frequency | Crit Band rate | Absolute threshold
       TH = [34.13   0.850  25.87   ;
            93.75      0.925    24.17  ;  187.50    1.842    13.87;
            281.25     2.742    10.01  ;  375.00    3.618     7.94;
            468.75     4.463     6.62  ;  532.50    5.272     5.70;
            656.25     6.041     5.00  ;  750.00    6.770     4.45;
            843.75     7.457     4.00  ;  937.50    8.103     3.61;
            1031.25    8.708     3.26  ;  1125.00   9.275     2.93;
            1218.75    9.805     2.63  ;  1312.50   10.301    2.32;
            1406.25    10.765    2.02  ;  1500.00   11.199    1.71;
            1593.75    11.606    1.38  ;  1687.50   11.988    1.04;
            1781.25    12.347    0.67  ;  1875.00   12.684    0.29;
            1968.75    13.002   -0.11  ;  2062.50   13.302   -0.54;
            2156.25    13.586   -0.97  ;  2250.00   13.855   -1.43;
            2343.75    14.111   -1.88  ;  2437.50   14.354   -2.34;
            2531.25    14.585   -2.79  ;  2625.00   14.807   -3.22;
            2718.75    15.018   -3.62  ;  2812.50   15.221   -3.98;
            2906.25    15.415   -4.30  ;  3000.00   15.602   -4.57;
            3093.75    15.783   -4.77  ;  3187.50   15.956   -4.91;
            3281.25    16.124   -4.98  ;  3375.00   16.287   -4.97;
            3468.75    16.445   -4.90  ;  3562.50   16.598   -4.76;
            3656.25    16.746   -4.55  ;  3750.00   16.891   -4.29;
            3843.75    17.032   -3.99  ;  3937.50   17.169   -3.64;
            4031.25    17.303   -3.26  ;  4125.00   17.434   -2.86;
            4218.75    17.563   -2.45  ;  4312.50   17.688   -2.04;
            4406.25    17.811   -1.63  ;  4500.00   17.932   -1.24;
            4687.50    18.166   -0.51  ;  4875.00   18.392    0.12;
            5062.50    18.611    0.64  ;  5250.00   18.823    1.06;
            5437.50    19.028    1.39  ;  5625.00   19.226    1.66;
            5812.50    19.419    1.88  ;  6000.00   19.606    2.08;
            6187.50    19.788    2.27  ;  6375.00   19.964    2.46;
            6562.50    20.135    2.65  ;  6750.00   20.300    2.86;
            6937.50    20.461    3.09  ;  7125.00   20.616    3.33;
            7312.50    20.766    3.60  ;  7500.00   20.912    3.89;
            7687.50    21.052    4.20  ;  7875.00   21.188    4.54;
            8062.50    21.318    4.91  ;  8250.00   21.445    5.31;
            8437.50    21.567    5.73  ;  8625.00   21.684    6.18;
            8812.50    21.797    6.67  ;  9000.00   21.906    7.19;
            9375.00    22.113    8.33  ;  9750.00   22.304    9.63;
            10125.00   22.482    11.08 ;  10500.00  22.646   12.71;
            10875.00   22.799    14.53 ;  11250.00  22.941   16.54;
            11625.00   23.072    18.77 ;  12000.00  23.195   21.23;
            12375.00   23.309    23.94 ;  12750.00  23.415   26.90;
            13125.00   23.515    30.14 ;  13500.00  23.607   33.67;
            13875.00   23.694    37.51 ;  14250.00  23.775   41.67;
            14625.00   23.852    46.17 ;  15000.00  23.923   51.04;
            15375.00   23.991    56.29 ;  15750.00  24.054   61.94;
            16125.00   24.114    68.00 ;  16500.00  24.171   68.00;
            16875.00   24.224    68.00 ;  17250.00  24.275   68.00;
            17625.00   24.322    68.00 ;  18000.00  24.368   68.00;
            18375.00   24.411    68.00 ;  18750.00  24.452   68.00;
            19125.00   24.491    68.00 ;  19500.00  24.528   68.00;
            19875.00   24.564    68.00 ;  20250.00  24.597   68.00;
      ];
      
      
      N = length(TH(:, 1));
      
      % Convert frequencies to samples indecies.
      for i = 1:N,
         TH(i, INDEX) = round(TH(i, INDEX) / 48000 * 512+1);
      end
      


      % Generate a mapping between the FFT_SIZE / 2 samples of the input
      % signal and the N coefficients of the absolute threshold table.

      % Borders
      for j = 1:TH(1, INDEX),
         Map(j) = 1;
      end
      for j = TH(N, INDEX):FFT_SIZE/2,
         Map(j) = N;
      end
      % All the other (from table)
      for i = 2:N-1,
         for j = TH(i, INDEX):TH(i+1, INDEX) - 1,
            Map(j) = i;
         end
      end
      
      % An offset depending on the overall bit rate is used for the absolute
      % threshold. This offset is -12dB for bit rates >=- 96kbits/s and 0dB
      % for bit rates < 96 kbits/s per channel. [1, pp. 111]
      if (bitrate >= 96)
         for i = 1:N,
           TH(i, ATH) = TH(i, ATH) - 0;
         end
      end
      
      LTq = TH(:, ATH);
   else
      error('Frequency not supported.');
   end
else
   error('Layer not supported.');
   
   end
