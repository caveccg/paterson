function [SBark] = Table_absolute_threshold_bark_48(Layer, fs)
%[TH, Map,  LTq] = Table_absolute_threshold(Layer, fs, bitrate)
%
%   Returns the frequencies, critical band rates and absolute threshold
%   in TH. Map contais a mapping beween the frequency line k and an index
%   number for the TH or LTq tables. LTq contains only the threshold in quiet
%   LT_q(k) defined in tables D.1x of the standard [1, pp. 117].
%   
%   These values depends on the Layer, the frequency rate fs (H)z and the
%   bit rate `bitrate' kbits/s.
%
%   Conversion of the frequency f into barks is done using (f * bitrate / fs)
   
%   Author: Fabien A. P. Petitcolas
%           Computer Laboratory
%           University of Cambridge
%
%   Copyright (c) 1998--2001 by Fabien A. P. Petitcolas
%   $Header: /Matlab MPEG/Table_absolute_threshold.m 3     7/07/01 1:27 Fabienpe $
%   $Id: Table_absolute_threshold.m,v 1.2 1998-06-22 17:47:56+01 fapp2 Exp $

%   References:
%    [1] Information technology -- Coding of moving pictures and associated
%        audio for digital storage media at up to 1,5 Mbits/s -- Part3: audio.
%        British standard. BSI, London. October 1993. Implementation of ISO/IEC
%        11172-3:1993. BSI, London. First edition 1993-08-01.
%
%   Legal notice:
%    This computer program is based on ISO/IEC 11172-3:1993, Information
%    technology -- Coding of moving pictures and associated audio for digital
%    storage media at up to about 1,5 Mbit/s -- Part 3: Audio, with the
%    permission of ISO. Copies of this standards can be purchased from the
%    British Standards Institution, 389 Chiswick High Road, GB-London W4 4AL, 
%    Telephone:+ 44 181 996 90 00, Telefax:+ 44 181 996 74 00 or from ISO,
%    postal box 56, CH-1211 Geneva 20, Telephone +41 22 749 0111, Telefax
%    +4122 734 1079. Copyright remains with ISO.
%-------------------------------------------------------------------------------
Common;

if (Layer == 1)
   if (fs == 48000)
      % Frequency | Crit Band rate | Absolute threshold
     SBark = [  0  86.13   ; 
            1  93.75   ; 2   187.50  ;
            3  281.25  ; 4   375.00  ;
            5  468.75  ; 6   562.50  ;
            7  656.25  ; 8   843.75  ;  
            9  937.50  ; 10   1125.00 ;  
            11 1312.50 ; 12  1500.00 ;
            13 1781.25 ; 14  1968.75 ;
            15 2342.75 ; 16  2718.75 ; 
            17 3281.25 ; 18  3845.75 ;  
            19 4687.50 ; 20  5437.50 ;
            21 6375.00 ; 22  7687.50 ;  
            23 9375.00 ; 24  11625.00; 
            26 15375.00; 27  20250.00;
      ];
      
    end 
    end

   