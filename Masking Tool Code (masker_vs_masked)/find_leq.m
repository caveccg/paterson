function [T1, audiogain_dB, audio_proc_L_dB, maxdbL,Leqdb] = find_leq(audio, weight,cal, Fs);


%2ch separation
audioCh1=audio;


%In order to get calibrated signals, get cal offset values from aarae
caldB_lft= cal; %usar valor 145


% apply caliration gain on audio
gainL = caldB_lft;


audioCh1gain = audioCh1 * 10.^(gainL/20); %applies a gain that is specified in dB(level offset)

audiogain (:,1)=audioCh1gain;


%weighting (a or z)
%weight = 'a';

%integration time seconds
tau = 0.125; 

%audio mag to dB
audiogain_dB=20*log10(abs(audiogain)); %digital (FS) mag to dB levels


%% calc Leq (a or z weighting)

% Left channel
[len,chans] = size(audioCh1gain);
fs=Fs;



audio_proc_L=audioCh1gain;

%apply weighting
audio_proc_L = weighting(audio_proc_L,Fs,weight);

% square and apply temporal integration
if tau > 0
    % apply tmporal integration so that percentiles can be derived
    % FILTER DESIGN
    E = exp(-1/(tau*fs)); % exponential term
    b = 1 - E; % filter numerator (adjusts gain to compensate for denominator)
    a = [1, -E];% filter denominator
    
    % rectify, integrate and square
    %audio=filter(b,a,abs(audio)).^2;
    
    % integrate the squared wave
    audio_proc_L=filter(b,a,audio_proc_L.^2);    
else
    % no temporal integration
    audio_proc_L = audio_proc_L.^2;
end

Leq_L = 10*log10(mean(audio_proc_L)); %note that audio has already been squared before
Leq_L = permute(Leq_L,[3,2,1]);

Leq_L1 = real(Leq_L);
Leqdb = round(Leq_L1); 

Lmax_L = 10*log10(max(audio_proc_L));
Lmax_L = permute(Lmax_L,[3,2,1]);

maxdbL1 = real(Lmax_L);
maxdbL = round(maxdbL1); 

%Plot SPL vs Time
N1 = length(audioCh1);% sample lenth
T1 = linspace(0, N1/Fs, N1);
audio_proc_L_dB=10*log10(audio_proc_L);%note that audio has already been squared before




endfunction