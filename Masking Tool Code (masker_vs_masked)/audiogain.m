function [audiogain1,audiogain2] = audiogain (y, nivel);
  

audio = y;

%% process recording
[l, ch]=size(audio);
if ch <2
    audio(:,2)= audio (:,1);    
   
end

%2ch separation
audioCh1=audio(:,1);
audioCh2=audio(:,2);%right channel if existent

%In order to get calibrated signals, get cal offset values from aarae
caldB_lft= nivel;
caldB_rgt= nivel;

% apply caliration gain on audio
gainL = caldB_lft;
gainR = caldB_rgt;

audioCh1gain = audioCh1 * 10.^(gainL/20); %applies a gain that is specified in dB(level offset)
audioCh2gain = audioCh2 * 10.^(gainR/20);
audiogain (:,1)=audioCh1gain;
audiogain (:,2)=audioCh2gain;

audiogain1=audiogain (:,1);
audiogain2=audiogain (:,2);


endfunction