function [DFlags, DTonal_list, DNon_tonal_list] = ...
   Decimation_new(X, Tonal_list, Non_tonal_list, Flags, TH, Map)
%[DFlags, DTonal_list, DNon_tonal_list] = ...
%   Decimation(X, Tonal_list, Non_tonal_list, Flags, TH, Map)
%
%   Components which are below the auditory threshold or are less than one 
%   half of a critical band width from a neighbouring component are
%   eliminated [1, pp. 113]. DFlags, DTonal_list and DNon_tonal_list
%   contain the list of flags, tonal components and non-tonal components after
%   decimation.
%
%   See also Find_tonal_components
   
%
%   References:
%    [1] Information technology -- Coding of moving pictures and associated
%        audio for digital storage media at up to 1,5 Mbits/s -- Part3: audio.
%        British standard. BSI, London. October 1993. Implementation of ISO/IEC
%        11172-3:1993. BSI, London. First edition 1993-08-01.
%
%   Legal notice:
%    This computer program is based on ISO/IEC 11172-3:1993, Information
%    technology -- Coding of moving pictures and associated audio for digital
%    storage media at up to about 1,5 Mbit/s -- Part 3: Audio, with the
%    permission of ISO. Copies of this standards can be purchased from the
%    British Standards Institution, 389 Chiswick High Road, GB-London W4 4AL, 
%    Telephone:+ 44 181 996 90 00, Telefax:+ 44 181 996 74 00 or from ISO,
%    postal box 56, CH-1211 Geneva 20, Telephone +41 22 749 0111, Telefax
%    +4122 734 1079. Copyright remains with ISO.
%-------------------------------------------------------------------------------
Common_new;

DFlags = Flags; % Flags after decimation


% Tonal or non-tonal components are not considered if lower than
% the absolute threshold of hearing found in TH(:, ATH).

% Non tonal case
DNon_tonal_list = [];
if not(isempty(Non_tonal_list))
	for i = 1:length(Non_tonal_list(:, 1)),
	   k = Non_tonal_list(i, INDEX);
	   %if (k > length(Map))
	   %   DFlags(k) = IRRELEVANT;
	   %else
	   if (Non_tonal_list(i, SPL) < TH(Map(k), ATH))
           DFlags(k) = IRRELEVANT;
		else
		   DNon_tonal_list = [DNon_tonal_list; Non_tonal_list(i, :)];
	   end
	   %end
	end

end


% Tonal case 
% Step 5a p. 113
DTonal_list = [];
if not(isempty(Tonal_list))
	for i = 1:length(Tonal_list(:, 1)),
	   k = Tonal_list(i, INDEX);
	   %if (k > length(Map))
	   %   DFlags(k) = IRRELEVANT;
	   %else
	   if (Tonal_list(i, SPL) < TH(Map(k), ATH))
		   DFlags(k) = IRRELEVANT;
	   else
		   DTonal_list = [DTonal_list; Tonal_list(i, :)];
	   end
	   %end
	end

end

% Eliminate tonal components that are less than one half of
% critical band width from a neighbouring component.

if not(isempty(DTonal_list))
    i = 1;
    while (i < length(DTonal_list(:, 1)))
        k      = DTonal_list(i, INDEX);
        k_next = DTonal_list(i + 1, INDEX);
        if (TH(Map(k_next), BARK) - TH(Map(k), BARK) < 0.5)
            if (DTonal_list(i, SPL) < DTonal_list(i + 1, SPL))
                DTonal_list = DTonal_list([1:i - 1, ...
                        i + 1:length(DTonal_list(:, 1))], :);
                DFlags(k) = IRRELEVANT;
           else
               DTonal_list = DTonal_list([1:i, ...
                       i + 2:length(DTonal_list(:, 1))], :);
               DFlags(k_next) = IRRELEVANT;
           end
       else
           i = i + 1;
       end
   end

end

