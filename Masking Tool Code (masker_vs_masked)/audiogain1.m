function [audiogain1] = audiogain1 (y, nivel);
  

audio = y;

%2ch separation
audioCh1=audio(:,1);

%In order to get calibrated signals, get cal offset values from aarae
caldB_lft= nivel;

% apply caliration gain on audio
gainL = caldB_lft;

audioCh1gain = audioCh1 * 10.^(gainL/20); %applies a gain that is specified in dB(level offset)

audiogain (:,1)=audioCh1gain;

audiogain1=audiogain (:,1);

endfunction